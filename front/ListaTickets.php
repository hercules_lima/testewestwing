<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema de Tickets</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function(){
            $('#inseriu').hide();
            $('#erro1').hide();
        })
        function cadTicket(){
            var nome = $('#nome').val();
            var email = $('#email').val();
            var pedido = $('#pedido').val();
            var titulo = $('#titulo').val();
            var conteudo = $('#cont').val();
            //alert(nome+' '+email+' '+pedido+' '+titulo+' '+conteudo);
            $.post("../back/cadastro/cadTicket.php?acao=cadastrar&nomeCliente="+nome+"&email="+email+"&pedido="+pedido+"&titulo="+titulo+"&conteudo="+conteudo+"",
            function( result ) {
                if(result == 1){
                    $('#erro1').show();
                }else if(result == 2){
                    $('#msgerro').html('Pedido pertence à outro cliente!');
                    $('#erro1').show();
                }else{
                    $('#inseriu').show();
                }
            });

        }
        function filtro(){
            var numeroPedido = $('#filtroPedido').val();
            var email = $('#filtroEmail').val();

            window.location = "ListaTickets.php?acao=filtrar&numeroPedido="+numeroPedido+"&email="+email+"";
        }
        function detalhes(pedidoId, pedidoNumero, pedidoTitulo, clienteId, clienteNome, clienteEmail, ticketId, ticketData, ticketConteudo){
           
            $('#detTicket').val(ticketId);
            $('#detPedido').val(pedidoNumero);
            $('#detDtCriacao').val(ticketData);
            $('#detNome').val(clienteNome);
            $('#detEmail').val(clienteEmail);
            $('#detTitulo').val(pedidoTitulo);
            $('#detCont').val(ticketConteudo);
        }
    </script>
    <style>
        .gridLista{
            position: relative;
            width: 80%;
            height: calc(100% - 38px);
            margin: 0 auto;
            top: 100px;
            border-radius: 3px;
            
        }
        body{
            background-color: orange;
        }
        button{
            float: right;
            margin-right: 5px;
        }
        
        nav{
            float: left;
        }
    </style>
</head>
<body>
    <form>
        <div class="gridLista">
            <h1>Sistema de Tickets</h1>
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                    <th scope="col">Número do ticket</th>
                    <th scope="col">Número do pedido</th>
                    <th scope="col">Título do Pedido</th>
                    <th scope="col">E-mail do cliente</th>
                    <th scope="col">Data de criação do ticket</th>
                    <th scope="col">Ver Detalhes</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include("../config/config.php");
                    $select = "SELECT  P.PEDIDO_ID,
                                        P.PEDIDO_NUMERO,
                                        P.PEDIDO_TITULO,
                                        C.CLIENTE_ID,
                                        C.CLIENTE_NOME,
                                        C.CLIENTE_EMAIL,
                                        T.TICKET_ID,
                                        T.TICKET_DTCRIACAO,
                                        T.TICKET_CONTEUDO
                                FROM PEDIDO P
                                JOIN TICKET T ON(P.TICKET_ID = T.TICKET_ID)
                                JOIN CLIENTE C ON(C.CLIENTE_ID = P.CLIENTE_ID)
                                WHERE P.PEDIDO_ID > 0
                            ";
                    if(isset($_REQUEST['acao']) &&  $_REQUEST['acao'] == 'filtrar'){
                        if(isset($_REQUEST['numeroPedido']) and trim($_REQUEST['numeroPedido']) <> ''){
                            $select.= " AND P.PEDIDO_NUMERO = '".$_REQUEST['numeroPedido']."'";
                        }
                        if(isset($_REQUEST['email']) and trim($_REQUEST['email']) <> ''){
                            $select.= " AND C.CLIENTE_EMAIL = '".$_REQUEST['email']."'";
                        }
                    }
                    $select .= " ORDER BY P.PEDIDO_ID DESC";
                    $res = mysqli_query($con, $select);
                    $total_reg = 5;
                    $pagina=1;
                    if(isset($_REQUEST['pagina'])){
                        $pagina=$_REQUEST['pagina'];
                    }                    
                    if (!$pagina) {
                        $pc = "1";
                    } else {
                        $pc = $pagina;
                    }
                    $inicio = $pc - 1;
                    $inicio = $inicio * $total_reg;
                    $limite = mysqli_query($con,"$select LIMIT $inicio,$total_reg");
                    $todos = mysqli_query($con,"$select");
                    $tr = mysqli_num_rows($todos);
                    $tp = $tr / $total_reg;
                    while ($row = mysqli_fetch_row($limite)) {
                        echo "
                        <tr>
                        <th scope=\"row\">" . $row[6] . "</th>
                        <td>" . $row[1] . "</td>
                        <td>" . $row[2] . "</td>
                        <td>" . $row[5] . "</td>
                        <td>" . $row[7] . "</td>
                        <td><a class=\"btn-lg\" data-toggle=\"modal\" data-target=\"#myModal1\" onClick=\"detalhes('".$row[0]."', '".$row[1]."', '".$row[2]."', '".$row[3]."', '".$row[4]."', '".$row[5]."', '".$row[6]."', '".$row[7]."', '".$row[8]."');\"><img src=\"../img/detalhes1.png\"></img></a></td>
                        </tr>
                        ";
                    }
                    ?>
                </tbody>
            </table>
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        <?php
                            $anterior = $pc -1;
                            $proximo = $pc +1;
                                if ($pc>1) {
                                    echo '
                                        <a class="page-link" href="ListaTickets.php?pagina='.$anterior.'" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    ';
                                } 
                                
                        ?>
                    </li>                       
                    <li class="page-item">
                        <?php
                            if ($pc<$tp) {
                                echo '
                                    <a class="page-link" href="ListaTickets.php?pagina='.$proximo.'" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                ';
                            }
                        ?>
                    </li>
                </ul>
            </nav>
            <button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#myModal" id="#myModal">Cadastrar</button>
            <button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal2" id="#myModal2">Filtrar</button>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro de Tickets</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="location.reload();">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="alert alert-success alert-dismissible" id="inseriu">
                    <strong>Tickete Inserido!</strong>Ticket cadastrado com sucesso.
                </div>
                <div class="alert alert-danger" id="erro1">
                    <strong>Erro!</strong> Ticket não inserido.<div id="msgerro"></div>
                </div>
                <div class="form-group">
                    <label for="nome"><b>Nome do Cliente:</b></label>
                    <input type="text" class="form-control" id="nome" placeholder="ex: João Pedro">
                </div>
                <div class="form-group">
                    <label for="email"><b>E-mail:</b></label>
                    <input type="email" class="form-control" id="email" placeholder="ex: joao@email.com">
                </div>
                <div class="form-group">
                    <label for="pedido"><b>Número do Pedido:</b></label>
                    <input type="number" class="form-control" id="pedido" placeholder="ex: 12345">
                </div>
                <div class="form-group">
                    <label for="titulo"><b>Título do Pedido:</b></label>
                    <input type="text" class="form-control" id="titulo" placeholder="ex: Móveis para área externa">
                </div>
                <div class="form-group">
                    <label for="cont"><b>Conteúdo do ticket:</b></label>
                    <textarea type="text" class="form-control" id="cont" rows="3"></textarea>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onClick="cadTicket()">Salvar</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalhes do Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                    <label for="nome"><b>Número do Ticket:</b></label>
                    <input type="text" class="form-control" id="detTicket" value="" readonly>
                    <label for="nome"><b>Número do Pedido:</b></label>
                    <input type="text" class="form-control" id="detPedido" readonly>
                    <label for="nome"><b>Data de Criação:</b></label>
                    <input type="text" class="form-control" id="detDtCriacao" readonly>
                </div>
                <div class="form-group">
                    <label for="nome"><b>Nome do Cliente:</b></label>
                    <input type="text" class="form-control" id="detNome" readonly>
                </div>
                <div class="form-group">
                    <label for="email"><b>E-mail:</b></label>
                    <input type="email" class="form-control" id="detEmail" readonly>
                </div>
                <div class="form-group">
                    <label for="titulo"><b>Título do Pedido:</b></label>
                    <input type="text" class="form-control" id="detTitulo" readonly>
                </div>
                <div class="form-group">
                    <label for="cont"><b>Conteúdo do ticket:</b></label>
                    <textarea type="text" class="form-control" id="detCont" rows="3" readonly></textarea>
                </div>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filtrar Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                    <label for="email"><b>E-mail:</b></label>
                    <input type="email" class="form-control" id="filtroEmail" placeholder="ex: joao@email.com">
                </div>
                <div class="form-group">
                    <label for="pedido"><b>Número do Pedido:</b></label>
                    <input type="number" class="form-control" id="filtroPedido" placeholder="ex: 12345">
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onClick="filtro()">Filtrar</button>
                </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>